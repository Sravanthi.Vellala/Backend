﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using API.Functions;
using Models.Models;


namespace API.Controllers
{
    public class AssociateDetailsController : ApiController
    {
        private MyDbContext db = new MyDbContext();
        private VolunteerFunctions functions = new VolunteerFunctions();

        // GET: api/AssociateDetails
        //[Authorize]
        // [Route("api/AssociateDetails")]
        [HttpGet, Route("api/AssociateDetails")]
        [ResponseType(typeof(AssociateDetails))]
        public IQueryable<AssociateRoles> GetAssociateDetails()
        {
            var identity = (ClaimsIdentity)User.Identity;
            //return db.AssociateDetails;
             return functions.GetAssociateRoles();
        }

        // GET: api/AssociateDetails/5
        [HttpGet, Route("api/AssociateDetails/{id}")]
        [ResponseType(typeof(AssociateDetails))]
        public IHttpActionResult GetAssociateDetails([FromUri]int id)
        {
            AssociateDetails associateDetails = db.AssociateDetails.Find(id);
            if (associateDetails == null)
            {
                return NotFound();
            }

            return Ok(associateDetails);
        }

        //[HttpGet, Route("api/AssociateDetails/LoginDetails/{email}/{password}")]
        //[ResponseType(typeof(AssociateDetails))]
        //public IHttpActionResult GetAssociateLoginDetails([FromUri]string email, [FromUri]string password)
        //{
        //    AssociateDetails associateDetails = db.AssociateDetails.Find(email);
        //    if (associateDetails == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(associateDetails);
        //}

        [ResponseType(typeof(AssociateDetails))]
        [HttpGet, Route("api/AssociateDetails/LoginDetails/{email}")]
        public IHttpActionResult GetAssociateLoginDetails([FromUri]string email)
        {
            IQueryable<AssociateRoles> associateDetails = functions.GetAssociateRole(email);
            if (associateDetails == null)
                return NotFound();
            return Ok(associateDetails);
        }


        //[HttpGet, Route("api/AssociateDetails/GetAssociateLoginDetails/{email}/{password}")]
        //[ResponseType(typeof(AssociateRoles))]
        //public IHttpActionResult GetAssociateLoginDetails([FromUri]string email, [FromUri]string password)
        //{
        //    //return db.AssociateDetails;
        //    return functions.GetAssociateRoles(email, password);
        //}


        //[HttpGet, Route("api/AssociateDetails/GetAssociateLoginDetails/{email}/{password}")]
        //[ResponseType(typeof(AssociateDetails))]
        //public IQueryable<AssociateRoles> GetAssociateLoginDetails([FromUri]string email, [FromUri]string password)
        //{
        //    //return db.AssociateDetails;
        //    return functions.GetAssociateRoles(email,password);
        //}

        // GET: api/AssociateDetails/5
        [ResponseType(typeof(AssociateDetails))]
        [Route("api/AssociateDetails/associateDetailsByEvent/{eventId}")]
        public IHttpActionResult GetAssociateDetailsByEvent(int eventId)
        {
            IQueryable<AssociateDetails> associateDetails = functions.GetAssociateDetailsByEventId(eventId);
            if (associateDetails == null)
                return NotFound();
            return Ok(associateDetails);
        }
        [ResponseType(typeof(AssociateDetails))]
        [Route("api/AssociateDetails/volunteerHrsBy/{employeeID}")]
        public IHttpActionResult GetVolunteerHours(int employeeId)
        {
            int volunteerHrs = functions.GetVolunteerHrsbyEmpid(employeeId);           
            return Ok(volunteerHrs);
        }

        //[HttpGet, Route("api/AssociateDetails/UserRoles/{email}")]
        //[ResponseType(typeof(AssociateDetails))]
        //public IQueryable<AssociateRoles> GetUserRoles([FromUri]string email)
        //{
        //    return functions.GetAssociateRole(email);
        //}

        // PUT: api/AssociateDetails/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAssociateDetails(int id, AssociateDetails associateDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != associateDetails.Id)
            {
                return BadRequest();
            }

            db.Entry(associateDetails).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssociateDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AssociateDetails
        [HttpPost, Route("api/AssociateDetails/SaveAssociateDetails")]
        [ResponseType(typeof(AssociateDetails))]
        public IHttpActionResult PostAssociateDetails(AssociateDetails associateDetails)
        {
            //if (!ModelState.IsValid)
            //{
            //    
            //return BadRequest(ModelState);
            //}

            //db.AssociateDetails.Add(associateDetails);
            //db.SaveChanges();

            //return CreatedAtRoute("DefaultApi", new { id = associateDetails.Id }, associateDetails);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                try
                {
                    functions.CreateAssociateDetails(associateDetails);
                    var msg = Request.CreateResponse(HttpStatusCode.Created);
                    msg.Headers.Location = new Uri(Request.RequestUri + associateDetails.Id.ToString());
                    return CreatedAtRoute("DefaultApi", new { id = associateDetails.Id }, associateDetails);

                }
                catch (Exception ex)
                {
                    //throw new Exception("error",ex.Message);
                    Console.WriteLine(ex.Message.ToString());
                    throw new Exception("error");
                }
            }
        }

        // DELETE: api/AssociateDetails/5
        [ResponseType(typeof(AssociateDetails))]
        public IHttpActionResult DeleteAssociateDetails(int id)
        {
            AssociateDetails associateDetails = db.AssociateDetails.Find(id);
            if (associateDetails == null)
            {
                return NotFound();
            }

            db.AssociateDetails.Remove(associateDetails);
            db.SaveChanges();

            return Ok(associateDetails);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AssociateDetailsExists(int id)
        {
            return db.AssociateDetails.Count(e => e.Id == id) > 0;
        }

        [ResponseType(typeof(AssociateDetails))]
        [HttpGet,Route("api/AssociateDetails/GetBusinessUnitDetails")]
        public IHttpActionResult GetBusinessUnitDetails()
        {
            var businessUnitId = db.BusinessUnits.Distinct().ToList();
            if (businessUnitId == null)
            {
                return NotFound();
            }

            return Ok(businessUnitId);
        }

        [ResponseType(typeof(AssociateDetails))]
        [HttpGet, Route("api/AssociateDetails/GetTransportTypeDetails/{eventId}")]
        public IHttpActionResult GetTransportTypeDetails(int eventId)
        {
            var transportType = db.EventDetails.Include(r=>r.TransportType).FirstOrDefault(r => r.EventId == eventId);
            if (transportType == null || transportType.TransportType == null)
            {
                return NotFound();
            }
            return Ok(transportType.TransportType.TransportType);
        }
    }
}