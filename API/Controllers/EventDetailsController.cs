﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using API.Functions;
using API.Utility;
using Models.Models;

namespace API.Controllers
{
    public class EventDetailsController : ApiController
    {
        private MyDbContext db = new MyDbContext();
        private VolunteerFunctions functions = new VolunteerFunctions();
        // GET: api/EventDetails
        //[Authorize(Roles = "Admin,PMO")]
        [HttpGet,Route("api/EventDetails")]
        //[AllowAnonymous]
        public IQueryable<ViewEventDetails> GetEventDetails()
        {
            return functions.GetEventDetails();
        }
        [HttpGet, Route("api/EventDetails/GetEventInformation")]
        //[AllowAnonymous]
        public IQueryable<ViewEventDetails> GetEventInformation()
        {
            return functions.GetEventInformation();
        }

        [HttpGet, Route("api/EventDetails/GetEventSummary")]
        //[AllowAnonymous]
        public IQueryable<ViewEventDetails> GetEventSummary()
        {
            return functions.GetEventSummary();
        }

        // GET: api/EventDetails/5
        // [Authorize(Roles = "Admin,PMO")]
        [HttpGet, Route("api/EventDetails/GetEventDetailsById/{id}")]
        //[ResponseType(typeof(EventDetails))]

        public IHttpActionResult GetEventDetailsById([FromUri]int id)
        {
            int totalVolunteerRegistered = db.EventRegistrations.Where(r => r.EventId == id).Count();
            EventDetails eventdetails = db.EventDetails.FirstOrDefault(r => r.EventId == id);
            int VolRequired = 0;
            if (eventdetails != null)
            {
                VolRequired = eventdetails.VolunteersRequired;
            }
            if (totalVolunteerRegistered >= VolRequired)
            {
                return BadRequest("Kindly note that your registration to volunteer for this event has been waitlisted as the registration has been closed now." +
                                  "In case your registration gets confirmed at a later point,you will be informed through email."+
                                  "Thank You for sharing your willingness to volunteer.");
            }
            if (DateTime.Today > eventdetails.EventDate)
            {
                return BadRequest("Registration closed already");
            }
            var eventDetails = db.EventDetails.Where(e=>e.EventId==id);
            return Ok(eventDetails.ToList());
        }
        [HttpGet]
        [Route("api/EventDetails/GetEventDetailsByPocId")]
        //[ResponseType(typeof(EventDetails))]
        public IQueryable<ViewEventDetails> GetEventDetailsByPocId(string emailId)
        {
           

            var eventDetails = functions.GetEventListByDateAndPocid(emailId);
            //if (eventDetails == null)
            //{
            //    return NotFound();
            //}

            return eventDetails;
        }
        // [Route("api/EventDetails/notifyupcomingeventdetails/{toList}")]
        [HttpPost, Route("api/EventDetails/notifyupcomingeventdetails")]
        public IHttpActionResult NotifyUpcomingEventDetails(List<BusinessUnit> toList)
        {
            if (toList.Count()==0)
            {
                return BadRequest("Email to list cannnot be empty");
            }
            else
            {
                try
                {
                    functions.NotifyUpcomingEvents(toList);
                    return StatusCode(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    return StatusCode(HttpStatusCode.InternalServerError);
                }
            }
        }

        [HttpGet]
        [Route("api/EventDetails/eventdetailsbydate/{eventDate}")]
        public IQueryable<EventDetails> GetEventDetailsByEventDate(DateTime eventDate)
        {
            //return db.EventDetails.Where(r => r.EventDate == eventDate && r.EventStatus.ToUpper() == "APPROVED");
            var eventDetails = db.EventDetails.Where(r => r.EventDate == eventDate && r.EventStatus.ToUpper() == "APPROVED");
            //var eventNames = eventDetails.Select(c => c.EventName);
            return eventDetails;
        }

        [HttpGet]
        [Route("api/EventDetails/eventDetailsByPocAndDate")]
        public IQueryable<EventDetails> GetEventDetailsByPocAndEventDate(string emailId,DateTime eventDate)
        {
            //return db.EventDetails.Where(r => r.EventDate == eventDate && r.EventStatus.ToUpper() == "APPROVED");
            var eventDetails = functions.GetEventListByDateAndPocid(emailId,eventDate);
            //var eventNames = eventDetails.Select(c => c.EventName);
            return eventDetails;
        }


        [HttpGet]
        [Route("api/EventDetails/topLocationEventDetailsbyDate/{eventDate}")]
        public IQueryable<EventDetails> GetTopLocationEventList(DateTime eventDate)
        {
            //return db.EventDetails.Where(r => r.EventDate == eventDate && r.EventStatus.ToUpper() == "APPROVED");
            var eventDetails = functions.GetTopLocationList(eventDate);
            //var eventNames = eventDetails.Select(c => c.EventName);
            return eventDetails;
        }

        // GET: api/EventDetails/5/true
        [HttpGet,Route("api/EventDetails/FavEvents/{favEvent}")]
        //[HttpGet, Route("api/EventDetails/FavEvents")]
        [ResponseType(typeof(EventDetails))]
        // [Route("api/eventdetails/associateID/favEvent")]
        public IQueryable<ViewEventDetails> GetFavEventDetails([FromUri]bool favEvent)
        {
            return functions.GetFavEventDetails(favEvent);
        }


        [HttpPut,Route("api/EventDetails/UpdateEventDetails/{id}")]
        // PUT: api/EventDetails/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEventDetails([FromUri]int id, [FromBody]EventDetails eventDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != eventDetails.EventId)
            {
                return BadRequest();
            }

            db.Entry(eventDetails).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
                if(eventDetails != null && eventDetails.EventStatus != null &&  eventDetails.EventStatus.ToLower()== "rejected")
                    functions.NotifyEventRejectedToPOC(eventDetails);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //private string GetEventRejectedEmailBody(EventDetails eventDetails)
        //{
            
        //}

       
        //// POST: api/EventDetails
        //[ResponseType(typeof(EventDetails))]
        //public IHttpActionResult PostEventDetails(EventDetails eventDetails)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.EventDetails.Add(eventDetails);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = eventDetails.EventId }, eventDetails);
        //}

        [HttpPost,Route("api/EventDetails/SaveEventDetails")]
        [ResponseType(typeof(EventDetails))]

        //[Authorize(Roles = "Admin")]
        //[Route("api/EventDetails")]
        //HttpPostAttribute: api/EventDetails
        public IHttpActionResult PostEventDetails([FromBody]EventDetails eventDetails)
        {
            //if (String.IsNullOrWhiteSpace(eventDetails.EventName))
            //{
            //    //return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Event Name can not be empty");
            //    return NotFound();
            //}
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                try
                {
                    functions.CreateEventDetails(eventDetails);                    
                    var msg = Request.CreateResponse(HttpStatusCode.Created);
                    msg.Headers.Location = new Uri(Request.RequestUri + eventDetails.EventId.ToString());
                    return CreatedAtRoute("CreateSingleEventApi", new { id = eventDetails.EventId }, eventDetails);
                    //return msg;

                }
                catch (Exception ex)
                {
                    //throw new Exception("error",ex.Message);
                    Console.WriteLine(ex.Message.ToString());
                    throw new Exception("error");
                }
            }
        }

        [HttpPost, Route("api/EventDetails/BulkUploadData")]
        [ResponseType(typeof(List<EventDetails>))]
        //  [Route("bulkuploaddata")]
        //httppostattribute: api/eventdetails
        public IHttpActionResult PostBulkUploadEventDetails([FromBody]List<EventDetails> eventdetails)
        {
            //if (string.isnullorwhitespace(eventdetails.eventname))
            //{
            //    //return request.createerrorresponse(httpstatuscode.badrequest, "event name can not be empty");
            //    return notfound();
            //}
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                try
                {
                    bool status = false;
                    if (ModelState.IsValid)
                    {
                        functions.BulkUplaodEventDetails(eventdetails);
                        status = true;
                        return Ok(status);
                    }
                    else
                    {
                        return BadRequest(ModelState);
                    }

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message.ToString());
                    throw new Exception("error");
                }
            }
        }


        // DELETE: api/EventDetails/5
        //[HttpDelete, Route("DeleteCustomer/{customerID}")]
        //public void DeleteProduct([FromUri]string customerID)

        [ResponseType(typeof(EventDetails))]
        [HttpDelete, Route("api/EventDetails/DeleteFavEvents/{id}")]
        public IHttpActionResult DeleteEventDetails([FromUri]int id)
        {
            EventDetails eventDetails = db.EventDetails.Find(id);
            if (eventDetails == null)
            {
                return NotFound();
            }

            db.EventDetails.Remove(eventDetails);
            db.SaveChanges();

            return Ok(eventDetails);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EventDetailsExists(int id)
        {
            return db.EventDetails.Count(e => e.EventId == id) > 0;
        }

        // GET: api/BeneficiaryDetails
        [HttpGet,Route("api/EventDetails/BeneficiaryDetails")]
        public IHttpActionResult GetBeneficiaryDetails()
        {
            List<string> beneficiaryDetails = db.EventDetails.Select(x => x.BeneficiaryName).Distinct().ToList();
            if (beneficiaryDetails == null)
            {
                return NotFound();
            }

            return Ok(beneficiaryDetails);
        }

        // GET: api/CouncilDetails
        [HttpGet,Route("api/EventDetails/CouncilDetails")]
        public IHttpActionResult GetCouncilDetails()
        {
            List<string> councilName = db.EventDetails.Select(x => x.CouncilName).Distinct().ToList();
            if (councilName == null)
            {
                return NotFound();
            }

            return Ok(councilName);
        }
        // GET: api/ProjectDetails
        [HttpGet,Route("api/EventDetails/ProjectDetails")]
        public IHttpActionResult GetProjectDetails()
        {
            List<string> projectDetails = db.EventDetails.Select(x => x.ProjectName).Distinct().ToList();
            if (projectDetails == null)
            {
                return NotFound();
            }

            return Ok(projectDetails);
        }
        // GET: api/EventStatus
        [HttpGet,Route("api/EventDetails/EventCategory")]
        public IHttpActionResult GetEventCategoryDetails()
        {
            List<string> eventCategoryDetails = db.EventDetails.Select(x => x.EventCategory).Distinct().ToList();
            if (eventCategoryDetails == null)
            {
                return NotFound();
            }

            return Ok(eventCategoryDetails);
        }
        // GET: api/Location
        [HttpGet,Route("api/EventDetails/LocationDetails")]
        public IHttpActionResult GetLocationDetails()
        {
            List<string> locationDetails = db.EventDetails.Select(x => x.LocationName).Distinct().ToList();
            if (locationDetails == null)
            {
                return NotFound();
            }
            return Ok(locationDetails);
        }

        // GET: api/Location
        [HttpGet, Route("api/EventDetails/getChartData")]
        public IHttpActionResult GetChartData()
        {
            var chartData = functions.GetChartDataForVolunteerReq();
            if (chartData == null)
            {
                return NotFound();
            }
            return Ok(chartData);
        }
        // GET: api/Location
        [HttpGet, Route("api/EventDetails/getPieChartData")]
        public IHttpActionResult GetPieChartData()
        {
            var chartData = functions.GetPieChartData();
            if (chartData == null)
            {
                return NotFound();
            }
            return Ok(chartData);
        }
        // GET: api/Location
        [HttpGet, Route("api/EventDetails/getPieChartDataForPoc")]
        public IHttpActionResult GetPieChartDataForPocId(string emailId)
        {
            var chartData = functions.GetPocChartData(emailId);
            if (chartData == null)
            {
                return NotFound();
            }
            return Ok(chartData);
        }

        [HttpGet, Route("api/EventDetails/GetBuList")]
        // GET: api/Roles
        public IQueryable<BusinessUnit> GetBulistDetail()
        {
            var data = functions.GetBuList();
            return data;
        }

    }
}