﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Functions;
using Models.Models;

namespace API.Controllers
{
    public class FutureAvailabilitiesController : ApiController
    {
        private MyDbContext db = new MyDbContext();
        private VolunteerFunctions functions = new VolunteerFunctions();

        // GET: api/FutureAvailabilities
        [HttpGet, Route("api/FutureAvailability")]
       // [ResponseType(typeof(EventDetails))]
        public IQueryable<FutureAvailability> GetFutureAvailability()
        {
            return db.FutureAvailability;
        }

        // GET: api/FutureAvailabilities/5
        [ResponseType(typeof(FutureAvailability))]
        public IHttpActionResult GetFutureAvailability(int id)
        {
            FutureAvailability futureAvailability = db.FutureAvailability.Find(id);
            if (futureAvailability == null)
            {
                return NotFound();
            }

            return Ok(futureAvailability);
        }

        // PUT: api/FutureAvailabilities/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFutureAvailability(int id, FutureAvailability futureAvailability)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != futureAvailability.Id)
            {
                return BadRequest();
            }

            db.Entry(futureAvailability).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FutureAvailabilityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FutureAvailabilities
        [HttpPost, Route("api/FutureAvailability/SaveFutureAvailability")]
        [ResponseType(typeof(FutureAvailability))]
        public IHttpActionResult PostFutureAvailability([FromBody]FutureAvailability futureAvailability)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                try
                {
                    functions.CreateFutureAvailability(futureAvailability);
                    var msg = Request.CreateResponse(HttpStatusCode.Created);
                    msg.Headers.Location = new Uri(Request.RequestUri + futureAvailability.Id.ToString());
                    return CreatedAtRoute("DefaultApi", new { id = futureAvailability.Id }, futureAvailability);

                }
                catch (Exception ex)
                {
                    //throw new Exception("error",ex.Message);
                    Console.WriteLine(ex.Message.ToString());
                    throw new Exception("error");
                }
            }
        }

        // DELETE: api/FutureAvailabilities/5
        [ResponseType(typeof(FutureAvailability))]
        public IHttpActionResult DeleteFutureAvailability(int id)
        {
            FutureAvailability futureAvailability = db.FutureAvailability.Find(id);
            if (futureAvailability == null)
            {
                return NotFound();
            }

            db.FutureAvailability.Remove(futureAvailability);
            db.SaveChanges();

            return Ok(futureAvailability);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FutureAvailabilityExists(int id)
        {
            return db.FutureAvailability.Count(e => e.Id == id) > 0;
        }
    }
}