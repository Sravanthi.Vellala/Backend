﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Models.Models;
using System.Security.Authentication;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity.Owin;

namespace API.Controllers
{
    public class LoginController : ApiController
    {
        private MyDbContext db = new MyDbContext();

        // GET: api/Login
        public IQueryable<AssociateDetails> GetAssociateDetails()
        {
            return db.AssociateDetails;
        }

        // GET: api/Login/5
        [ResponseType(typeof(AssociateDetails))]
        public IHttpActionResult GetAssociateDetails(int id)
        {
            AssociateDetails associateDetails = db.AssociateDetails.Find(id);
            if (associateDetails == null)
            {
                return NotFound();
            }

            return Ok(associateDetails);
        }

        // PUT: api/Login/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAssociateDetails(int id, AssociateDetails associateDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != associateDetails.Id)
            {
                return BadRequest();
            }

            db.Entry(associateDetails).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssociateDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            //public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            //{
            //    if (identity == null)
            //    {
            //        return null;
            //    }

            //    Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

            //    if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
            //        || String.IsNullOrEmpty(providerKeyClaim.Value))
            //    {
            //        return null;
            //    }

            //    if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
            //    {
            //        return null;
            //    }

            //    return new ExternalLoginData
            //    {
            //        LoginProvider = providerKeyClaim.Issuer,
            //        ProviderKey = providerKeyClaim.Value,
            //        UserName = identity.FindFirstValue(ClaimTypes.Name)
            //    };
            //}
        }

        // POST: api/Login
        [ResponseType(typeof(AssociateDetails))]
        public IHttpActionResult PostAssociateDetails(AssociateDetails associateDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AssociateDetails.Add(associateDetails);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = associateDetails.Id }, associateDetails);
        }

        // DELETE: api/Login/5
        [ResponseType(typeof(AssociateDetails))]
        public IHttpActionResult DeleteAssociateDetails(int id)
        {
            AssociateDetails associateDetails = db.AssociateDetails.Find(id);
            if (associateDetails == null)
            {
                return NotFound();
            }

            db.AssociateDetails.Remove(associateDetails);
            db.SaveChanges();

            return Ok(associateDetails);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AssociateDetailsExists(int id)
        {
            return db.AssociateDetails.Count(e => e.Id == id) > 0;
        }
    }
}