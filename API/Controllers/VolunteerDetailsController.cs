﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Functions;
using Models.Models;

namespace API.Controllers
{
    public class VolunteerDetailsController : ApiController
    {
        private MyDbContext db = new MyDbContext();
        private VolunteerFunctions functions = new VolunteerFunctions();

        // GET: api/VolunteerDetails
        public IQueryable<VolunteerDetails> GetVolunteerDetails()
        {
            return db.VolunteerDetails;
        }

        // GET: api/VolunteerDetails/5
        [ResponseType(typeof(VolunteerDetails))]
        public IHttpActionResult GetVolunteerDetails(int id)
        {
            VolunteerDetails volunteerDetails = db.VolunteerDetails.Find(id);
            if (volunteerDetails == null)
            {
                return NotFound();
            }

            return Ok(volunteerDetails);
        }

        // PUT: api/VolunteerDetails/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVolunteerDetails(int id, VolunteerDetails volunteerDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != volunteerDetails.Id)
            {
                return BadRequest();
            }

            db.Entry(volunteerDetails).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VolunteerDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost, Route("api/VolunteerDetails/SaveVolunteerDetails")]
        // POST: api/VolunteerDetails
        [ResponseType(typeof(VolunteerDetails))]
        public IHttpActionResult PostVolunteerDetails([FromBody]VolunteerDetails volunteerDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }         

            else
            {
                try
                {
                    functions.CreateVolunteerDetails(volunteerDetails);
                    var msg = Request.CreateResponse(HttpStatusCode.Created);
                    msg.Headers.Location = new Uri(Request.RequestUri + volunteerDetails.Id.ToString());
                    return Ok(msg);

                }
                catch (Exception ex)
                {
                    //throw new Exception("error",ex.Message);
                    Console.WriteLine(ex.Message.ToString());
                    throw new Exception("error");
                }
            }
        }

        [HttpPost]
        // POST: api/VolunteerDetails
        [ResponseType(typeof(List<VolunteerDetails>))]
        [Route("api/VolunteerDetails/BulkUploadVolunteerDetails")]
        public IHttpActionResult PostBulkVolunteerDetails(List<VolunteerDetails> volunteerDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            else
            {
                try
                {
                    functions.BulkUploadVolunteerDetails(volunteerDetails);
                    var msg = Request.CreateResponse(HttpStatusCode.Created);
                    return Ok(msg);

                }
                catch (Exception ex)
                {
                    //throw new Exception("error",ex.Message);
                    Console.WriteLine(ex.Message.ToString());
                    throw new Exception("error");
                }
            }
        }






        //[HttpPost]
        //// POST: api/VolunteerDetails
        //[ResponseType(typeof(VolunteerDetails))]
        //[Route("api/VolunteerDetails")]
        //public IHttpActionResult PostVolunteerDetails()
        //{

        //    return Ok();
        //}

        // DELETE: api/VolunteerDetails/5
        [ResponseType(typeof(VolunteerDetails))]
        public IHttpActionResult DeleteVolunteerDetails(int id)
        {
            VolunteerDetails volunteerDetails = db.VolunteerDetails.Find(id);
            if (volunteerDetails == null)
            {
                return NotFound();
            }

            db.VolunteerDetails.Remove(volunteerDetails);
            db.SaveChanges();

            return Ok(volunteerDetails);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VolunteerDetailsExists(int id)
        {
            return db.VolunteerDetails.Count(e => e.Id == id) > 0;
        }
    }
}