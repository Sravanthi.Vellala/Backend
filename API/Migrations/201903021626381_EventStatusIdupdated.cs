namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventStatusIdupdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventDetails", "EventStatusId", c => c.Int(nullable: false));
            CreateIndex("dbo.EventDetails", "EventStatusId");
            AddForeignKey("dbo.EventDetails", "EventStatusId", "dbo.EventStatus", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EventDetails", "EventStatusId", "dbo.EventStatus");
            DropIndex("dbo.EventDetails", new[] { "EventStatusId" });
            DropColumn("dbo.EventDetails", "EventStatusId");
        }
    }
}
