// <auto-generated />
namespace API.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class latestchanges : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(latestchanges));
        
        string IMigrationMetadata.Id
        {
            get { return "201903181717546_latestchanges"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
