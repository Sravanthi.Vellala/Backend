namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RoelsUpdated : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.AssociateDetails", name: "Users_Id", newName: "UserRoles_Id");
            RenameIndex(table: "dbo.AssociateDetails", name: "IX_Users_Id", newName: "IX_UserRoles_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.AssociateDetails", name: "IX_UserRoles_Id", newName: "IX_Users_Id");
            RenameColumn(table: "dbo.AssociateDetails", name: "UserRoles_Id", newName: "Users_Id");
        }
    }
}
