namespace API.Migrations
{
    
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MyDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MyDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //var roles = new List<UserRoles>
            //{
            //    new UserRoles{RoleType="Admin"},
            //    new UserRoles{RoleType="PMO"},
            //    new UserRoles{RoleType="POC"}
            //};

            //roles.ForEach(x => context.UserRoles.Add(x));
            //context.SaveChanges();


            //var associates = new List<AssociateDetails>
            //{
            //    //new AssociateDetails{Id=718432, AssociateName="Sravanthi",ContactNumber=9962829534,EmailId="sravanthi.vellala@cognizant.com",RoleId=1},
            //    //new AssociateDetails{Id=777777, AssociateName="Krithika",ContactNumber=9962829534,EmailId="krithika@cognizant.com",RoleId=1},
            //    //new AssociateDetails{Id=666666,AssociateName="Saranya",ContactNumber=333333333,EmailId="saranya@cognizant.com",RoleId=3},
            //    //new AssociateDetails{Id=55555,AssociateName="Roshan",ContactNumber=44444444,EmailId="roshan.vellala@cognizant.com",RoleId=2},
            //    //new AssociateDetails{Id=444444,AssociateName="Roshni",ContactNumber=555555555,EmailId="roshni.vellala@cognizant.com",RoleId=1},
            //    //new AssociateDetails{Id=33333,AssociateName="Sanju",ContactNumber=666666666,EmailId="sanju.vellala@cognizant.com",RoleId=3},
            //    //new AssociateDetails{Id=2222222,AssociateName="Charan",ContactNumber=777777777777,EmailId="charan.vellala@cognizant.com",RoleId=2},
            //    //  new AssociateDetails{Id=111122,AssociateName="Jashu",ContactNumber=99999999,EmailId="jashu.vellala@cognizant.com",RoleId=2}
            //};
            //associates.ForEach(x => context.AssociateDetails.Add(x));
            //context.SaveChanges();

           
        }
    }
}

