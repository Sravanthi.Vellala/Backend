﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class OwinAuthDbContext : IdentityDbContext
    {
        //    public OwinAuthDbContext()
        //: base("OwinAuthDbContext")
        //    {
        //    }

        //public OwinAuthDbContext()
        // : base("OwinAuthDbContext", throwIfV1Schema: false)
        //  public OwinAuthDbContext()
        //: base("OwinAuthDbContext")
        //  {
        //  }

        public OwinAuthDbContext()
: base("MyDbContext")
        {
        }

        public static OwinAuthDbContext Create()
        {
            return new OwinAuthDbContext();
        }
    }
}