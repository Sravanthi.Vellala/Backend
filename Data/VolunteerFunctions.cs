﻿using System;

namespace Data
{
    public class VolunteerFunctions
    {
        private AssociateDetailsContext db = new AssociateDetailsContext();

        // GET: api/AssociateDetails
        public IQueryable<AssociateRoles> GetAssociateRoles()
        {
            var data = (from ad in db.AssociateDetails
                        join
ur in db.UserRoles on ad.RoleId equals ur.Id
                        select new AssociateRoles
                        {
                            AssociateId = ad.Id,
                            AssociateName = ad.AssociateName,
                            ContactNumber = ad.ContactNumber,
                            EmailId = ad.EmailId,
                            RoleType = ur.RoleType
                        });
            return data;
        }

        public IQueryable<AssociateRoles> GetAssociateRolesByID(int id)
        {
            var data = (from ad in db.AssociateDetails
                        join ur in db.UserRoles on ad.RoleId equals ur.Id
                        where ur.Id == id
                        select new AssociateRoles
                        {
                            AssociateId = ad.Id,
                            AssociateName = ad.AssociateName,
                            ContactNumber = ad.ContactNumber,
                            EmailId = ad.EmailId,
                            RoleType = ur.RoleType
                        });
            return data;
        }

        public void CreateAssociateDetails(AssociateDetails associateDetails)
        {
            if (associateDetails == null)
            {
                throw new NotImplementedException("Not initialized");
            }
            else
            {
                associateDetails.AssociateName = "New NAME";
                associateDetails.ContactNumber = "12345";
                associateDetails.EmailId = "new mail id";
                db.AssociateDetails.Add(associateDetails);
                db.SaveChanges();
            }
        }

        public bool EditAssociateDetails(AssociateDetails associateDetails)
        {
            db.Entry(associateDetails).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }

        public void DeleteAssociateDetails(int id, AssociateDetails associateDetails)
        {
            var details = db.AssociateDetails.Where(t => t.AssociateName == associateDetails.AssociateName).ToList();
            if (details.Count > 0)
            {
                for (int i = 0; i < details.Count; i++)
                {
                    db.AssociateDetails.Remove(details[i]);
                }
                db.SaveChanges();
            }

        }
    }
}

