﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
   public class ChartData
    {
        [JsonProperty("month")]
        public string Month { get; set; }

        [JsonProperty("eventCount")]
        public int EventCount { get; set; }

        [JsonProperty("eventDate")]
        public DateTime EventDate { get; set; }

        [JsonProperty("registrationCount")]
        public int RegistrationCount { get; set; }

         [JsonProperty("projectTeam")]
        public string ProjectTeam { get; set; }
    }
}
