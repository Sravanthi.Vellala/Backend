﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
   public class UserRoles
    {
        [Key]
        [JsonProperty("id")]
        public int Id { get; set; }

        [Required]
        [JsonProperty("employeeID")]
        public int EmployeeID { get; set; }

        [Required]
        [JsonProperty("roleId")]
        public int RoleId { get; set; }

        [JsonProperty("roleType")]
        public string RoleType { get; set; }

        [JsonProperty("assosiates")]
        public ICollection<AssociateDetails> Associates { get; set; }

        [JsonProperty("roles")]
        public ICollection<Roles> Roles { get; set; }
    }
}
