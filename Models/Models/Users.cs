﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
   public class Users
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        [JsonProperty("employeeID")]
        public int EmployeeID { get; set; }
        [Required]
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [Required]
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [Required]
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [Required]
        [JsonProperty("contactNumber")]
        public long ContactNumber { get; set; }

        [Required]
        [JsonProperty("emailId")]
        public string EmailId { get; set; }

        [Required]
        [JsonProperty("password")]
        public string Password { get; set; }

        public UserRoles UserRoles { get; set; }
    }
}
