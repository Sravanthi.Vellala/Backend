﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class ViewEventRegistrations
    {

        public int Id { get; set; }
        public int AssociateDetailsId { get; set; }
        public int EventId { get; set; }
        public int BusinessUnitId { get; set; }
        public int ParticipationId { get; set; }
        public bool WaitingList { get; set; }
        public int VolunteerHours { get; set; }
        public int TravelHours { get; set; }
        public int LivesImpacted { get; set; }
        public string EventCode { get; set; }
        public string LocationName { get; set; }
        public string BeneficiaryName { get; set; }
        public string CouncilName { get; set; }
        public string EventName { get; set; }
        public string EventDescription { get; set; }
        public DateTime EventDate { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public long ContactNumber { get; set; }
        public string Email { get; set; }
        [JsonProperty("businessUnit")]
        public string BusineeUnit { get; set; }
        public string EventStatus { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        [JsonProperty("topTeamCount")]
        public int TopTeamCount { get; set; }
    }
}
