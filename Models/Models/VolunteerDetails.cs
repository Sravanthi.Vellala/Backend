﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
  public class VolunteerDetails
    {
        [Key]
        [JsonProperty("Id")]
        public int Id { get; set; }

        //[JsonProperty("associateDetailsId")]
        //public int AssociateDetailsId { get; set; }

        [JsonProperty("eventId")]
        public int EventId { get; set; }

        //[JsonProperty("eventStatusId")]
        //public int EventStatusId { get; set; }

        [JsonProperty("volunteerHours")]
        public int VolunteerHours { get; set; }

        [JsonProperty("travelHours")]
        public int TravelHours { get; set; }

        [JsonProperty("livesImpacted")]
        public int LivesImpacted { get; set;  }

        [NotMapped]
        [JsonProperty("employeeName")]
        public string EmployeeName { get; set; }

        //[NotMapped]
        [JsonProperty("employeeID")]
        public int EmployeeID { get; set; }

        
        [JsonProperty("participationID")]
        public int ParticipationId { get; set; }

        [NotMapped]
        [JsonProperty("contactNumber")]
        public long ContactNumber { get; set; }

        [NotMapped]
        [JsonProperty("emailId")]
        public string EmailId { get; set; }

        //  public EventStatus Status { get; set; }        
        public EventDetails Event { get; set; }
        public AssociateDetails Associate { get; set; }
    }
}
